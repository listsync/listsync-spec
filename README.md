# listsync-spec

*Not actively maintained.*

The API specification for the listsync list-sharing system.

## The spec file

The specification (in OpenAPI 3.x format) is in [listsync.yaml](listsync.yaml).

## Browse online

To interact with the spec using the Swagger UI, go to
[listsync.gitlab.io/listsync-spec](https://listsync.gitlab.io/listsync-spec/).

## Browse locally

To interact with the spec using Swagger UI on your own machine, run:

```bash
make update-swagger-ui
make run
```

and then open http://0.0.0.0:8000/ in your web browser.

By default, this will attempt to connect to the public instance at
[listsync.artificialworlds.net](https://listsync.artificialworlds.net),
*which has been disabled*.

## Software

For a server that implements this spec, try
[listsync-server-rust](https://gitlab.com/listsync/listsync-server-rust).

For a client that lets you write lists and save them to a server, try
[listsync-client-rust](https://gitlab.com/listsync/listsync-client-rust)

## License

Copyright (c) 2020 Andy Balaam, released under the Apache 2.0 License.

See [LICENSE](LICENSE) for more information.
