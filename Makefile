all: run

run:
	python3 -m http.server

update-swagger-ui:
	wget 'https://github.com/swagger-api/swagger-ui/archive/v3.25.0.tar.gz' \
		-O swagger-ui.tar.gz
	tar -xzf swagger-ui.tar.gz
	rm swagger-ui.tar.gz
	rm -rf swagger-ui
	mv swagger-ui-* swagger-ui
